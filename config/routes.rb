Vadmin::Engine.routes.draw do
  resources :envs, only: [:index, :update]
  resources :email_notifications do
    collection do
      get  :resend_password_reset
      get  :resend_confirmation
    end
  end
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  delete '/oauth-mgmt/sign_out', as: 'logout'
end
