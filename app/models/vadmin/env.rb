module Vadmin
  class Env
    ENV_LOCATION = ENV['ENV_LOCATION'] || '.env'
    ENV_FILE = Rails.root + ENV_LOCATION
    attr_accessor :name, :value

    def initialize name, value
      @name = name
      @value = value
    end

    # Save all variables and replace current one
    def save
      output = ""
      self.class.all.each do |item|
        item.value = value if item.name == name
        output += "#{item.name}=#{item.value}\n"
      end
      write output
    end

    # Write string into file
    def write output
      File.open(ENV_FILE, 'w') { |file| file.write(output) }
    end

    class << self
      # Read file
      def read
        File.read(ENV_FILE)
      end

      # Read all environment variables
      def all
        read.split("\n").map do |line|
          new line.split("=").first, line.split("=").second
        end
      end
    end
  end
end
