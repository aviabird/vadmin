require_dependency "vadmin/application_controller"

module Vadmin
  class EmailNotificationsController < ApplicationController
    before_action :set_talent_auth, only: [:resend_password_reset, :resend_confirmation]

    def resend_password_reset
      if @talent_auth.present?
        @talent_auth.send_reset_password_instructions
        flash[:notice] = 'Password reset email sent successfully'
      else
        flash[:notice] = 'Talent auth doesnt exists Email sending failed'
      end
      redirect_to :back
    end

    def resend_confirmation 
      if @talent_auth.present?
        @talent_auth.send_confirmation_instructions
        flash[:notice] = 'Confirmation email sent successfully'
      else
        flash[:notice] = 'Talent auth doesnt exists Email sending failed'
      end
      redirect_to :back
    end

    private

    def set_talent_auth
      talent = Talent.where(id: params[:talent_id]).first
      @talent_auth = talent.auth
    end
  end
end
