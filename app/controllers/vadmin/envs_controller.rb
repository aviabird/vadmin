require_dependency "vadmin/application_controller"

module Vadmin
  class EnvsController < ApplicationController
    before_action :set_env, only: [:show, :edit, :update, :destroy]

    # GET /envs
    def index
      @envs = Env.all
    end

    # PATCH/PUT /envs/1
    def update
      if @env.save
        redirect_to envs_path, notice: 'Env was successfully updated.'
      else
        render :edit
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_env
        @env = Env.new(env_params[:name], env_params[:value])
      end

      # Only allow a trusted parameter "white list" through.
      def env_params
        params.require(:env).permit(:name, :value)
      end
  end
end
