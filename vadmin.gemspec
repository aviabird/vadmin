$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "vadmin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "vadmin"
  s.version     = Vadmin::VERSION
  s.authors     = ["jtrichereau"]
  s.email       = ["admin@talentsolutions.at"]
  s.homepage    = "http://talentsolutions.at"
  s.summary     = "myVeeta admin interface"
  s.description = "myVeeta admin interface based on Rails Engine"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.1"
  s.add_dependency "rails_admin"
  s.add_dependency "simple_form"
  s.add_dependency "bootstrap-sass"


  s.add_development_dependency "sqlite3"
  s.add_development_dependency "pry"

end
