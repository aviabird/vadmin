
  module RailsAdmin
    module Config
      module Actions
        class EmailNotification < RailsAdmin::Config::Actions::Base
          # Register itself
          RailsAdmin::Config::Actions.register(self)

          # This ensures the action only shows up for Users
          register_instance_option :visible? do
            bindings[:object].class == Talent
          end
          
          # We want the action on members, not the Users collection
          register_instance_option :member do
            true
          end
          register_instance_option :link_icon do
            'icon-envelope'
          end
          # You may or may not want pjax for your action
          register_instance_option :pjax? do
            false
          end

          register_instance_option :controller do
            Proc.new do
              # Note: This is dummy code. The thing to note is that we aren't
              # rendering a view, just redirecting after taking an action on @object, which
              # will be the user instance in this case.
              # redirect_to back_or_index
            end
          end
        end
      end
    end
  end
