require "rails_admin"
require "simple_form"
require "bootstrap-sass"

module Vadmin
  class Engine < ::Rails::Engine
    isolate_namespace Vadmin
  end
end
